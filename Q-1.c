#include<stdio.h>
#include<string.h>
// C program to reverse a sentence entered by user.
int main()
{
	int x,y;
	char sentence[1000];
	
	printf("Enter your sentence: ");
	gets(sentence);
	
	int size=strlen(sentence);
	
	 printf("Reversed sentence: ");
	 
	 for(y=size;y>=0;y--)
	 {
	 	printf("%c",sentence[y]);
	 }
		
	return 0;
}
